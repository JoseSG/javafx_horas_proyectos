package com.example.horasproyectos.controllers;

import com.example.horasproyectos.InitialApplication;

import com.example.horasproyectos.utilities.Encriptacion;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.*;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;


public class FondosController implements Initializable{

    @FXML
    public ImageView ivFondo1;
    @FXML
    public ImageView ivFondo2;
    @FXML
    public ImageView ivFondo3;
    @FXML
    public ImageView ivFondo4;
    @FXML
    public ImageView ivFondo5;
    @FXML
    public ImageView ivFondo6;

    public static String[] urlImagenesUo = new String[4];
    private int contadorImages = 0;

    public File copiaCodificada;
    public WritableImage[] writableImages = new WritableImage[3];
    public static boolean cambioDeFondo = false;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Al inicializar esta pantalla, leemos el archivo que había llamado configuración.txt
        String linea = leerLineaDeArchivo();
        //Y separamos cada ruta que contenga dicho archivo
        String[] rutas = anyadeRutas(linea);

        if (rutas != null) {
            //En este bucle obtenemos las imágenes codificadas y se las asignamos a los ImageView
            for (int i = 0; i < rutas.length; i++) {
                if (rutas[i] != null) {
                    try {
                        File copiaFile = new File(rutas[i]);
                        this.writableImages[i] = Encriptacion.decodificar(copiaFile);
                    } catch (FileNotFoundException ex){
                        ex.printStackTrace();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
            ivFondo2.setImage(writableImages[0]);
            ivFondo4.setImage(writableImages[1]);
            ivFondo6.setImage(writableImages[2]);
        }

    @FXML
    //Solo es necesario 1 método de DragOver para las 3 imágenes, gastan el mismo
    //Cuando se termina el evento de arrastre (Método necesario para arrastrar y soltar items)
    void onDragOver(DragEvent event) {
        Dragboard db = event.getDragboard();
        if (db.hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY);
        } else {
            event.consume();
        }
    }

    @FXML
    //Cuando se suelta un objeto después de arrastrar (Método necesario para arrastrar y soltar items)
    void onDragDropped(DragEvent event) throws IOException {
        Dragboard db = event.getDragboard();
        //Cogemos el archivo que se ha soltado, que se corresponde con una imagen
        File file = db.getFiles().get(0);
        //Y se la pasamos al método asigna imagen (para sacar su id), junto con el archivo soltado
        asignaImagen((ImageView) event.getSource(), file);
    }

    /**Este método solamente lee 1 línea de un archivo, la de configuracion.txt*/
    public static String leerLineaDeArchivo(){
        BufferedReader bf = null;
        String linea = "";
        try {

            bf = new BufferedReader(new FileReader("./src/main/resources/com/example/horasproyectos/copias/configuracion.txt"));
            linea = bf.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return linea;
    }

    //De esta forma, evitas tener que hacer 3 onDragDropped distintos, uno para cada imagen
    /**Este método asigna al momento la imagen arrastrada por el usuario al elemento que corresponda*/
    private void asignaImagen(ImageView imageView, File file) throws IOException {

        //me aseguro de que contador de imágenes no sea nunca mayor que 3 (porque solo hay 3 imageView donde meterlas)
        contadorImages = (contadorImages == 3) ? 0 : contadorImages;
        //Obtengo el # exacto del ID del ImageView para ponerle ese nombre al .txt codificado que se genera
        String imageViewId = imageView.getId();
        String id = imageViewId.substring(imageViewId.lastIndexOf("o"), imageViewId.length()).replace("o", "");
        //Generamos una copia codificada del File que recibimos (creamos un .txt)
        this.copiaCodificada = Encriptacion.codificar(
                file, Encriptacion.URL_IMAGENES_DECODIFICADAS + id + ".txt", this.copiaCodificada);
        //Metemos la ruta absoluta del archivo codificado en un array de String llamado urlImagenesUo
        String rutaAboluta = this.copiaCodificada.getAbsolutePath();
        String rutaRelativa = rutaAboluta.replace(rutaAboluta.substring(0, rutaAboluta.indexOf("src") - 1), ".");
        urlImagenesUo[contadorImages] = rutaRelativa;
        //Y decimos que el array de urlImagenUo no está vacío
        InitialApplication.urlImagenesUoVacio = false;
        //El objetivo es que se guarde la configuración que ponga el uo

        //Extraigo la imagen del archivo que nos ha arrastrado el uo
        Image imagen = new Image(file.toURI().toString());
        //Y se la asigno al ImageView que corresponda, en el que la haya soltado
        switch (imageView.getId()){
            case "ivFondo6":
                ivFondo6.setImage(imagen);
                break;
            case "ivFondo4":
                ivFondo4.setImage(imagen);
                break;
            case "ivFondo2":
                ivFondo2.setImage(imagen);
                break;
        }
        //contador necesario para ir añadiendo elementos al array de urlImagenesUo
        contadorImages++;
    }

    @FXML
    public void onFondoClick1() throws IOException {
        InitialApplication.idFondo = 1;
        InitialApplication.fondo = ivFondo1;
        cambioDeFondo = true;
        InitialApplication.abrirStageInicial(new Stage(), "update", null, this.getClass().toString(), true);
    }

    @FXML
    public void onFondoClick2() throws IOException {
        if (ivFondo2.getImage() != null) {
            InitialApplication.idFondo = 2;
            InitialApplication.fondo = ivFondo2;
            cambioDeFondo = true;
            InitialApplication.abrirStageInicial(new Stage(), "update", null, this.getClass().toString(), true);
        }
    }

    @FXML
    public void onFondoClick3() throws IOException {
        InitialApplication.idFondo = 3;
        InitialApplication.fondo = ivFondo3;
        cambioDeFondo = true;
        InitialApplication.abrirStageInicial(new Stage(), "update", null, this.getClass().toString(), true);
    }

    @FXML
    public void onFondoClick4() throws IOException {
        if (ivFondo4.getImage() != null) {
            InitialApplication.idFondo = 4;
            InitialApplication.fondo = ivFondo4;
            cambioDeFondo = true;
            InitialApplication.abrirStageInicial(new Stage(), "update", null, this.getClass().toString(), true);
        }
    }

    @FXML
    public void onFondoClick5() throws IOException {
        InitialApplication.idFondo = 5;
        InitialApplication.fondo = ivFondo5;
        cambioDeFondo = true;
        InitialApplication.abrirStageInicial(new Stage(), "update", null, this.getClass().toString(), true);
    }
    @FXML
    public void onFondoClick6() throws IOException {
        if (ivFondo6.getImage() != null) {
            InitialApplication.idFondo = 6;
            InitialApplication.fondo = ivFondo6;
            cambioDeFondo = true;
            InitialApplication.abrirStageInicial(new Stage(), "update", null, this.getClass().toString(), true);
        }
    }

    /**Sistema ideado para cargar las imágenes de las rutas que hayan en configuracion.txt*/
    private String[] anyadeRutas(String linea){

        String[] rutas;

        if (!linea.contains("$")) {
            rutas = new String[1];
            rutas[0] = linea.substring(linea.indexOf("#"), linea.length()).replace("#", "");
        } else if (!linea.contains("%")) {
            rutas = new String[2];
            rutas[0] = linea.substring(linea.indexOf("#"), linea.indexOf("$"))
                    .replace("#", "");
            rutas[1] = linea.substring(linea.indexOf("$"), linea.length())
                    .replace("$", "");
        } else {
            rutas = new String[3];
            rutas[0] = linea.substring(linea.indexOf("#"), linea.indexOf("$"))
                    .replace("#", "");
            rutas[1] = linea.substring(linea.indexOf("$"), linea.indexOf("%"))
                    .replace("$", "");
            rutas[2] = linea.substring(linea.indexOf("%"), linea.length())
                    .replace("%", "");
        }
        return rutas;
    }
}

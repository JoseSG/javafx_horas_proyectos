package com.example.horasproyectos.controllers;

import com.example.horasproyectos.InitialApplication;
import com.example.horasproyectos.exceptions.*;
import com.example.horasproyectos.models.*;
import com.example.horasproyectos.utilities.BridgeAPI;
import com.example.horasproyectos.utilities.Consultas;
import com.example.horasproyectos.utilities.CreaObjetos;
import com.example.horasproyectos.utilities.Regex;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//TODO Para que funcione bien el tema de mostrar los mensajes por pantalla informando al usuario: *******
// Siempre que se le mande una excepción al método mostrarInfoEfimera() no hay que mandarle una clase
// Y siempre que se haga un create, update o delete, NO hay que mandarle una excepción pero SI la clase del item creado

public class InitialController implements Initializable {

    //Constructor
    public InitialController() {

        try {
            //Obtengo de forma segura la ip y el puerto para conectar
            Map<String, String> datos = obtenerDatos("datos.txt");
            //TODO: Si tienes la api ejecutándose desde la máquina cliente... (el portátil)
            // Sustituye ip_publica por ip_local y puerto_abierto por puerto_local
            this.consultas = new Consultas(datos.get("ip_publica"), datos.get("puerto_abierto"));
        } catch (IOException e) {
            System.err.println("Error. No se han podido obtener los datos para la conexión");
        }
    }

    private Consultas consultas = null;

    //TODO De aquí para abajo es el código de la pestaña "Gestión de proyectos"

    @FXML
    public TextField tfNombreNewProject;
    @FXML
    public TextArea taDescripNewProject;
    @FXML
    public TextField tfIdProject;
    @FXML
    public Text tInfo;
    @FXML
    public Text tInforme;
    //Al principio tenía el otro ChoiceBox (cbEmpleados) como String también, pero tuve que cambiarlo porque si había 2 empleados con
    // el mismo nombre, nunca aparecían los datos del segundo, por eso me vi obligado a crear el ChoiceBox que contenga objetos Empleado,
    // el ChoiceBox mostrará por pantalla el toString de la clase que contenga.
    //En el de cbProyectos no he tenido ningún problema porque he considerado que no puede haber 2 proyectos con el mismo nombre
    // dejo ambos ChoiceBox (tanto el de String como el de Empleado) así tal cual como ejemplo para ver como se haría en ambos casos.
    @FXML
    public ChoiceBox<String> cbProyectos;
    @FXML
    public AnchorPane apProyectos;
    @FXML
    public AnchorPane apEmpleados;
    @FXML
    public Pane pBottom;
    @FXML
    public ImageView ivFondo;

    @FXML
    public BorderPane bpPadre;

    public static StringBuilder info = new StringBuilder("");
    public static ObservableList<Proyecto> listadoProyectos;
    public Proyecto proyecto;

    private boolean mismoNombre = false;
    private boolean error = false;
    private boolean warning = false;

    private StringBuilder informacion = new StringBuilder("");

    private final double TAMANYO_MAX_PANE_BOTTOM = 2200.0;

    private int clicksTable = 0;


    @FXML
    protected void onCrearBtClick() throws IOException {

        informacion.delete(0, informacion.length());
        tInforme.setText(informacion.toString());

        if (tabEmpleados.isSelected()) {

            compruebaLimites(tfNombreNewEmpleado, null);

            if (!error) {
                gestionDeErrores("crear", true);
                if (!warning) {
                    //gracias a .trim() quitamos los espacios y así comprobamos si la cadena está vacía
//                    if (tfNombreNewEmpleado.getText().trim().isEmpty()) {
//                        System.out.println("nombre de empleado vacío");
//                    }
                    for (Empleado empleado : listadoEmpleados) {
                        if (empleado.getTelefono().equals(tfTelfNewEmpleado.getText())) {
                            try {
                                throw new MismoTelefonoEmpleado();
                            } catch (MismoTelefonoEmpleado e) {
                                mismoTelefono = true;
                                error = true;
                                mostrarInfoEfimera("error", e, null, InitialApplication.stageInitial.getScene().getWidth());
                            }
                        }
                    }
                }
                //Si no ha saltado la excepción del proyecto con el mismo nombre, creamos uno nuevo.
                if (!mismoTelefono && !warning && !error) {

                    Consultas.createEmpleado(tfNombreNewEmpleado.getText(), tfTelfNewEmpleado.getText());

                    Empleado empleadoNuevo = new Empleado();
                    empleadoNuevo.setNombreempleado(tfNombreNewEmpleado.getText());
                    empleadoNuevo.setTelefono(tfTelfNewEmpleado.getText());

                    this.empleado = empleadoNuevo;

                    InitialApplication.abrirStageInicial(new Stage(), "create", null, empleadoNuevo.getClass().toString(), true);
                }
                eliminaErrores();
            }
        } else {

            compruebaLimites(tfNombreNewProject, taDescripNewProject);

            if (!error) {
                gestionDeErrores("crear", false);

                if (!warning) {
                    for (Proyecto proyecto : listadoProyectos) {
                        if (proyecto.getNombre().equals(tfNombreNewProject.getText())) {
                            try {
                                throw new MismoNombreProyecto();
                            } catch (MismoNombreProyecto e) {
                                mismoNombre = true;
                                error = true;
                                mostrarInfoEfimera("error", e, null, InitialApplication.stageInitial.getScene().getWidth());
                            }
                        }
                    }
                }
            }
            //Si no ha saltado la excepción del proyecto con el mismo nombre, creamos uno nuevo.
            if (!mismoNombre && !warning && !error) {

                Consultas.createProyecto(tfNombreNewProject.getText(), taDescripNewProject.getText());

                Proyecto proyectoNuevo = new Proyecto();
                proyectoNuevo.setNombre(tfNombreNewProject.getText());
                proyectoNuevo.setDescripcion(taDescripNewProject.getText());

                this.proyecto = proyectoNuevo;

                InitialApplication.abrirStageInicial(new Stage(), "create", null, proyectoNuevo.getClass().toString(), true);
            }
            eliminaErrores();
        }
    }

    @FXML
    protected void onActualizarBtClick() throws IOException {

        informacion.delete(0, informacion.length());
        tInforme.setText(informacion.toString());

        eliminaErrores();
        compruebaProyecto();

        if (!warning) {
            if (tabEmpleados.isSelected()) {

                gestionDeErrores("actualizar", true);

                if (!warning) {
                    if (this.empleado.getNombreempleado().equals(tfNombreNewEmpleado.getText()) &&
                            this.empleado.getTelefono().equals(tfTelfNewEmpleado.getText())) {
                        try {
                            throw new UpdateSinCambios();
                        } catch (UpdateSinCambios e) {
                            warning = true;
                            mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                        }
                    } else {
                        Consultas.updateEmpleado(this.empleado, tfNombreNewEmpleado.getText(), tfTelfNewEmpleado.getText());
                        ObservableList<Empleado> list = obtenerEmpleados();
                        mostrarEmpleado(list);

                        Empleado empleado = new Empleado();
                        empleado.setEmpleadoid(Integer.parseInt(tfIdEmpleado.getText()));
                        empleado.setNombreempleado(tfNombreNewEmpleado.getText());
                        empleado.setTelefono(taDescripNewProject.getText());

                        InitialApplication.abrirStageInicial(new Stage(), "update", null, empleado.getClass().toString(), true);
                    }
                }
            } else {

                gestionDeErrores("actualizar", false);

                if (!warning) {
                    if (this.proyecto.getNombre().equals(tfNombreNewProject.getText()) && this.proyecto.getDescripcion().equals(taDescripNewProject.getText())) {
                        try {
                            throw new UpdateSinCambios();
                        } catch (UpdateSinCambios e) {
                            warning = true;
                            mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                        }
                    } else {
                        Consultas.updateProyecto(this.proyecto, tfNombreNewProject.getText(), taDescripNewProject.getText());
                        ObservableList<Proyecto> list = obtenerProyectos();
                        mostrarProyecto(list);

                        Proyecto proyecto = new Proyecto();
                        proyecto.setProyectoid(Integer.parseInt(tfIdProject.getText()));
                        proyecto.setNombre(tfNombreNewProject.getText());
                        proyecto.setDescripcion(taDescripNewProject.getText());

                        InitialApplication.abrirStageInicial(new Stage(), "update", null, proyecto.getClass().toString(), true);
                    }
                }
            }
        }
        eliminaErrores();
    }

    @FXML
    protected void onEliminarBtClick() throws IOException {

        eliminaErrores();
        compruebaProyecto();

        if (!warning) {
            InitialApplication.stageConfirmacion = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(InitialApplication.class.getResource("confirmacion-usuario-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 464, 184);

            ConfirmationController confirmationController = (ConfirmationController) fxmlLoader.getController();

            if (tabEmpleados.isSelected())
                confirmationController.recibeEmpleado(this.empleado);
            else
                confirmationController.recibeProyecto(this.proyecto);

            InitialApplication.stageConfirmacion.setTitle("Confirmación de eliminación del registro de un proyecto");
            InitialApplication.stageConfirmacion.setScene(scene);
            InitialApplication.stageConfirmacion.show();
        }
    }

    /**
     * Este método carga los datos del ChoiceBox cada vez que se añade, modifica o se elimina un proyecto
     */
    public ObservableList<Proyecto> obtenerProyectos() {

        ObservableList<String> listNombres = FXCollections.observableArrayList();
        ObservableList<Proyecto> listProyectos = FXCollections.observableArrayList();
        String resultado = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_PROYECTOS);

        JSONArray jsonArray = new JSONArray(resultado);
        //Bucle para generar un objeto proyecto para cada objeto que nos devuelve la query
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject item = jsonArray.getJSONObject(i);

            Proyecto proyecto = CreaObjetos.creaProyectos(item, null);

            listNombres.add(proyecto.getNombre());
            listProyectos.add(proyecto);
        }

        cbProyectos.setItems(listNombres);
        listadoProyectos = listProyectos;

        return listProyectos;
    }


    @FXML
    public void onValidarBtClick() {

        eliminaErrores();

        //Si no se ha seleccionado el tab de Empleados, estaremos en el tab de Proyectos
        if (tabEmpleados.isSelected()) {

//TODO Así cogeríamos el objeto empleado seleccionado en el ChoiceBox o el índice (La posición en la que está)
//System.out.println(cbEmpleados.getSelectionModel().getSelectedItem());
//System.out.println(cbEmpleados.getSelectionModel().getSelectedIndex());

            for (Empleado listadoEmpleado : listadoEmpleados) {
                if (cbEmpleados.getValue().getEmpleadoid() == listadoEmpleado.getEmpleadoid()) {
                    tfNombreNewEmpleado.setText(listadoEmpleado.getNombreempleado());
                    tfTelfNewEmpleado.setText(listadoEmpleado.getTelefono());
                    tfIdEmpleado.setText(String.valueOf(listadoEmpleado.getEmpleadoid()));

                    Empleado empleadoSeleccionado = new Empleado();
                    empleadoSeleccionado.setEmpleadoid(listadoEmpleado.getEmpleadoid());
                    empleadoSeleccionado.setNombreempleado(listadoEmpleado.getNombreempleado());
                    empleadoSeleccionado.setTelefono(listadoEmpleado.getTelefono());

                    this.empleado = empleadoSeleccionado;
                    break;
                }
            }
        } else {

            for (Proyecto listadoProyecto : listadoProyectos) {

                if (cbProyectos.getValue().equals(listadoProyecto.getNombre())) {
                    tfNombreNewProject.setText(listadoProyecto.getNombre());
                    taDescripNewProject.setText(listadoProyecto.getDescripcion());
                    tfIdProject.setText(String.valueOf(listadoProyecto.getProyectoid()));

                    Proyecto proyectoSeleccionado = new Proyecto();
                    proyectoSeleccionado.setProyectoid(listadoProyecto.getProyectoid());
                    proyectoSeleccionado.setNombre(listadoProyecto.getNombre());
                    proyectoSeleccionado.setDescripcion(listadoProyecto.getDescripcion());

                    this.proyecto = proyectoSeleccionado;
                    break;
                }
            }
        }
    }

    /**
     * Este método lo usaba antes para limpiar todos los campos, ahora que reinicio el stage cada vez que actualizo datos en la BD no se gasta
     */
    public void limpiaCampos() {

        tfNombreNewProject.setText("");
        taDescripNewProject.setText("");
        tfIdProject.setText("");

        tfHoras.setText("");
        tfHorasNombreEmpleado.setText("");
        tfHorasNombreProyecto.setText("");

        tfHorasNombreProyecto.setText("");
        tfHorasNombreEmpleado.setText("");
        tfHoras.setText("");
    }

    /**
     * A este método se le llama cuando necesitamos mostrar 1 proyecto en concreto por pantalla
     */
    public void mostrarProyecto(ObservableList<Proyecto> listProyectos) {

        if (this.proyecto == null) {
            this.proyecto = listProyectos.get(0);
        }

        for (Proyecto unProyecto : listProyectos) {
            if (this.proyecto.getProyectoid().equals(unProyecto.getProyectoid())) {
                cbProyectos.setValue(unProyecto.getNombre());

                tfNombreNewProject.setText(unProyecto.getNombre());
                taDescripNewProject.setText(unProyecto.getDescripcion());
                tfIdProject.setText(String.valueOf(unProyecto.getProyectoid()));
                break;
            }
        }
    }

    /**
     * En la sección negra de la aplicación, este método muestra la información de las acciones del usuario en un hilo aparte en segundo plano
     */
    public void mostrarInfoEfimera(String proceso, Exception exception, String clase, double anchoScene) {

        //El tamaño de la Scene es 40 Px mayor que el del Pane pBottom, uso el tamaño del Scene para evitar errores desde Confirmation
        ajustaTextoAPantalla(anchoScene - 40);

        if (exception != null) {
            error = true;
        } else
            error = false;

        asignaInfo(proceso, exception, clase);

        tInfo.setText(info.toString());

        if (warning) {
            tInfo.setFill(Paint.valueOf("0xfff600ff"));// amarillo = Scene Builder --> #fff600 IntelliJ --> 0xfff600ff
        } else if (error) {
            tInfo.setFill(Paint.valueOf("0xcc0000ff")); // rojo = Scene Builder --> #cc0000 IntelliJ --> 0xcc0000ff
        } else
            tInfo.setFill(Paint.valueOf("0x29cd00ff")); // verde= IntelliJ --> 0x29cd00ff Scene Builder --> #29cd00

        //  System.out.println(tInfo.getFill());

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        //ejecutamos el hilo del executor en el que se informa al usuario, tInfo mostrará un mensaje informativo durante 3 segundos
        executorService.execute(new Runnable() {
            @Override
            public void run() {

                tInfo.setVisible(true);

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                tInfo.setVisible(false);
            }
        });
    }

    /**
     * Asigna la información a un atributo estático Stringbuilder de esta clase para posteriormente mostrar el mensaje
     * en el método mostrarInfoEfimera()
     */
    public void asignaInfo(String proceso, Exception exception, String clase) {

        info.delete(0, info.length());

        if (exception == null) {
            switch (clase) {
                case "class com.example.horasproyectos.models.Proyecto":
                    switch (proceso) {
                        case "create" -> info.append("Nuevo proyecto creado con éxito");
                        case "update" -> info.append("Proyecto seleccionado actualizado con éxito");
                        case "delete" -> info.append("Proyecto eliminado con éxito");
                        case "warning", "error" -> info.append(exception.getMessage());
                    }
                    break;
                case "class com.example.horasproyectos.models.Participan":
                    switch (proceso) {
                        case "create" -> info.append("Horas incurridas con éxito");
                        case "update" -> info.append("Registro de horas actualizado con éxito");
                        case "delete" -> info.append("Registro de horas eliminado con éxito");
                        case "warning", "error" -> info.append(exception.getMessage());
                    }
                    break;
                case "class com.example.horasproyectos.models.Empleado":
                    switch (proceso) {
                        case "create" -> info.append("Nuevo empleado creado con éxito");
                        case "update" -> info.append("Empleado seleccionado actualizado con éxito");
                        case "delete" -> info.append("Empleado eliminado con éxito");
                        case "warning", "error" -> info.append(exception.getMessage());
                    }
                    break;
                case "class com.example.horasproyectos.controllers.FondosController":
                    switch (proceso) {
                        //TODO de momento solo uso el update en esta sección
                        case "create" -> info.append("Nuevo fondo creado con éxito");
                        case "update" -> info.append("Fondo actualizado con éxito");
                        case "delete" -> info.append("Fondo eliminado con éxito");
                        case "warning", "error" -> info.append(exception.getMessage());
                    }
                    break;
            }
        } else {
            //Si se recibe alguna excepción, sea un error o un aviso, se mostrará el mensaje del exception recibido
            // también depende del mensaje
            if (exception.getMessage().equals("For input string: \"\"")) {
                warning = true;
                info.append("Por favor, complete los campos vacíos seleccionando registros de las tablas");
            } else if (exception.getMessage().equals("empty String")) {
                warning = true;
                info.append("Por favor, complete los campos vacíos seleccionando un registro de la tabla de incurrir horas");
            } else
                info.append(exception.getMessage());
        }
    }

    /**
     * Se llama en 2 botones, en el update y el delete para comprobar que se ha pulsado el botón de validar
     */
    public void compruebaProyecto() {
        if (!cbProyectos.getValue().equals(this.proyecto.getNombre())) {
            try {
                throw new DatosNoCoinciden();
            } catch (DatosNoCoinciden e) {
                warning = true;
                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
            }
        }
    }

    //TODO De aquí para abajo es el código de la pestaña "Tablas de datos"

    @FXML
    private TableView<Empleado> tvEmpleados = new TableView<>();
    @FXML
    private TableColumn<Empleado, Integer> tcEmpleadoId;
    @FXML
    private TableColumn<Empleado, String> tcEmpleadoNombre;
    @FXML
    private TableColumn<Empleado, String> tcEmpleadoTelf;

    ObservableList<Empleado> empleados;
    Empleado empleadoSeleccionado;

    @FXML
    private TableView<Proyecto> tvProyectos = new TableView<>();

    @FXML
    private TableColumn<Proyecto, Integer> tcProyectoId;
    @FXML
    private TableColumn<Proyecto, String> tcProyectoNombre;

    ObservableList<Proyecto> proyectos;
    Proyecto proyectoSeleccionado;

    @FXML
    private TableView<Participan> tvParticipan = new TableView<>();

    @FXML
    private TableColumn<Participan, Integer> tcParticipanRegistroId;
    @FXML
    private TableColumn<Participan, Integer> tcParticipanEmpleadoId;
    @FXML
    private TableColumn<Participan, Integer> tcParticipanProyectoId;
    @FXML
    private TableColumn<Participan, Float> tcParticipanHoras;

    ObservableList<Participan> participans;
    Participan participanSeleccionado;

    //Estos boolean son para saber en qué tabla se ha hecho click y borrar un botón u otro, así evitar muchos errores
    private boolean clickParticipanTabla = false;
    private boolean clickEmpleadoTabla = false;
    private boolean clickProyectoTabla = false;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        tInfo.setVisible(false);
        tInforme.setVisible(false);

        cambiarColorPane(InitialApplication.colorPane);

        creaMenuItem();

        //De esta forma hago que la imagen de fondo siempre tenga el mismo tamaño que el stage
//        ivFondo.setFitWidth(InitialApplication.stageInitial.getScene().getWidth());
//        ivFondo.setFitHeight(InitialApplication.stageInitial.getScene().getHeight());
//Pero la forma más óptima son estas dos lineas de código, como parámetro le pasamos el objeto padre que lo contiene
        ivFondo.fitWidthProperty().bind(bpPadre.widthProperty());
        ivFondo.fitHeightProperty().bind(bpPadre.heightProperty());

        if (InitialApplication.fondo != null) {
            this.ivFondo.setImage(InitialApplication.fondo.getImage());
        }


        empleadoSeleccionado = null;
        proyectoSeleccionado = null;

        //Con estas dos líneas consigo que el textArea haga un salto de linea para que muestre el texto a mi gusto
        taDescripNewProject.setPrefColumnCount(5);
        taDescripNewProject.setWrapText(true);

        cargaAllEmpleados();
        cargaAllProyectos();
        cargaAllParticipan();

    }

    /**
     * Este método inicializa la tabla de participan Table View y le pasa los valores
     */
    private void inicializarTablaParticipan() {
        tcParticipanRegistroId.setCellValueFactory(new PropertyValueFactory<Participan, Integer>("registroid"));
        tcParticipanEmpleadoId.setCellValueFactory(new PropertyValueFactory<Participan, Integer>("empleadoid"));
        tcParticipanProyectoId.setCellValueFactory(new PropertyValueFactory<Participan, Integer>("proyectoid"));
        tcParticipanHoras.setCellValueFactory(new PropertyValueFactory<Participan, Float>("horas"));

        participans = FXCollections.observableArrayList();
        tvParticipan.setItems(participans);
    }

    /**
     * Este método infla la lista añadiendo todos los participan de la BD
     */
    private void cargaAllParticipan() {

        String resultStream = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_PARTICIPAN);

        JSONArray jsonArray = new JSONArray(resultStream);
        inicializarTablaParticipan();
        //Bucle para generar un objeto participan para cada json que nos devuelve la query
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Participan participan = CreaObjetos.creaParticipan(jsonObject, null);

            participans.add(participan);
        }
    }

    /**
     * Este método inicializa la tabla de empleados Table View y le pasa los valores
     */
    private void inicializarTablaEmpleados() {
        tcEmpleadoId.setCellValueFactory(new PropertyValueFactory<Empleado, Integer>("empleadoid"));
        tcEmpleadoNombre.setCellValueFactory(new PropertyValueFactory<Empleado, String>("nombreempleado"));
        tcEmpleadoTelf.setCellValueFactory(new PropertyValueFactory<Empleado, String>("telefono"));

        empleados = FXCollections.observableArrayList();
        tvEmpleados.setItems(empleados);
    }

    /**
     * Este método infla la lista añadiendo todos los empleados de la BD
     */
    private void cargaAllEmpleados() {

        String resultStream = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_EMPLEADOS);

        JSONArray jsonArray = new JSONArray(resultStream);
        inicializarTablaEmpleados();
        //Bucle para generar un objeto usuario para cada objeto que nos devuelve la query
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Empleado empleado = CreaObjetos.creaEmpleados(jsonObject, null);

            empleados.add(empleado);
        }
    }

    /**
     * Este método inicializa la tabla de proyectos Table View y le pasa los valores
     */
    private void inicializarTablaProyectos() {
        tcProyectoId.setCellValueFactory(new PropertyValueFactory<Proyecto, Integer>("proyectoid"));
        tcProyectoNombre.setCellValueFactory(new PropertyValueFactory<Proyecto, String>("nombre"));

        proyectos = FXCollections.observableArrayList();
        tvProyectos.setItems(proyectos);
    }

    /**
     * Este método infla la lista añadiendo todos los proyectos de la BD
     */
    private void cargaAllProyectos() {

        String resultStream = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_PROYECTOS);

        JSONArray jsonArray = new JSONArray(resultStream);
        inicializarTablaProyectos();
        //Bucle para generar un objeto usuario para cada objeto que nos devuelve la query
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Proyecto proyecto = CreaObjetos.creaProyectos(jsonObject, null);

            proyectos.add(proyecto);
        }
    }

    @FXML
    protected void onEmpleadosTableClick() {
        this.empleadoSeleccionado = getTableEmpleadoSeleccionado();
        if (empleadoSeleccionado != null) {
            tfHorasNombreEmpleado.setText(empleadoSeleccionado.getNombreempleado());

            btModificar.setVisible(false);
            btBorrar.setVisible(false);
            btIncurrir.setVisible(true);

            clickEmpleadoTabla = true;
            clickParticipanTabla = false;

            String resultadoQuery = BridgeAPI.conection(Consultas.findByIdHorasEmpleado(this.empleadoSeleccionado.getEmpleadoid()));

            //Sistema ideado para que cada 3 clics se reinicie el texto y no se salga por abajo del Pane
            if ((clicksTable + 1) % 3 == 1) {

                tInforme.setText("");
                informacion.delete(0, informacion.length());

                informacion.insert(0, "ID empleado: " + this.empleadoSeleccionado.getEmpleadoid() +
                        "\nNombre del trabajador: " + this.empleadoSeleccionado.getNombreempleado() +
                        "\nTotal de horas trabajadas: " + resultadoQuery + "\n");
                tInforme.setText(informacion.toString());

                clicksTable = 0;
            }else{
                informacion.insert(0, "ID empleado: " + this.empleadoSeleccionado.getEmpleadoid() +
                        "\nNombre del trabajador: " + this.empleadoSeleccionado.getNombreempleado() +
                        "\nTotal de horas trabajadas: " + resultadoQuery + "\n---------------------------------\n");
                tInforme.setText(informacion.toString());
            }

            clicksTable++;
        } else
            tfHorasNombreEmpleado.setText("");
    }

    @FXML
    protected void onProyectosTableClick() {
        this.proyectoSeleccionado = getTableProyectoSeleccionado();
        if (proyectoSeleccionado != null) {
            tfHorasNombreProyecto.setText(proyectoSeleccionado.getNombre());

            btModificar.setVisible(false);
            btBorrar.setVisible(false);
            btIncurrir.setVisible(true);

            clickProyectoTabla = true;
            clickParticipanTabla = false;

            String resultadoQuery = BridgeAPI.conection(Consultas.findByIdHorasProyecto(this.proyectoSeleccionado.getProyectoid()));

            if ((clicksTable + 1) % 3 == 1) {

                tInforme.setText("");
                informacion.delete(0, informacion.length());

                informacion.insert(0, "ID proyecto: " + this.proyectoSeleccionado.getProyectoid() +
                        "\nNombre del proyecto: " + this.proyectoSeleccionado.getNombre() +
                        "\nTotal de horas invertidas: " + resultadoQuery + "\n");
                tInforme.setText(informacion.toString());

                clicksTable = 0;
            }else {
                informacion.insert(0, "ID proyecto: " + this.proyectoSeleccionado.getProyectoid() +
                        "\nNombre del proyecto: " + this.proyectoSeleccionado.getNombre() +
                        "\nTotal de horas invertidas: " + resultadoQuery + "\n---------------------------------\n");
                tInforme.setText(informacion.toString());
            }
            clicksTable++;
        } else
            tfHorasNombreProyecto.setText("");
    }

    @FXML
    protected void onParticipanTableClick() {
        this.participanSeleccionado = getTableParticipanSeleccionado();
        if (participanSeleccionado != null) {
            tfHorasNombreEmpleado.setText(participanSeleccionado.getEmpleado().getNombreempleado());
            tfHorasNombreProyecto.setText(participanSeleccionado.getProyecto().getNombre());
            tfHoras.setText(String.valueOf(participanSeleccionado.getHoras()));

            btIncurrir.setVisible(false);
            btModificar.setVisible(true);
            btBorrar.setVisible(true);

            clickParticipanTabla = true;
            clickEmpleadoTabla = false;
            clickProyectoTabla = false;

            String resultadoQuery = BridgeAPI.conection(Consultas.findByIdHorasParticipan(
                    this.participanSeleccionado.getEmpleadoid(), this.participanSeleccionado.getProyectoid()));

            if ((clicksTable + 1) % 3 == 1) {

                tInforme.setText("");
                informacion.delete(0, informacion.length());

                informacion.insert(0, "Proyecto: " + this.participanSeleccionado.getProyecto().getNombre() +
                        "\nEmpleado: " + this.participanSeleccionado.getEmpleado().getNombreempleado() +
                        "\nTotal de horas trabajadas: " + resultadoQuery + "\n");
                tInforme.setText(informacion.toString());
                clicksTable = 0;
            }else {
                informacion.insert(0, "Proyecto: " + this.participanSeleccionado.getProyecto().getNombre() +
                        "\nEmpleado: " + this.participanSeleccionado.getEmpleado().getNombreempleado() +
                        "\nTotal de horas trabajadas: " + resultadoQuery + "\n---------------------------------\n");
                tInforme.setText(informacion.toString());
            }

            clicksTable++;
        } else {
            tfHorasNombreEmpleado.setText("");
            tfHorasNombreProyecto.setText("");
            tfHoras.setText("");
        }
    }

    /**
     * Este método sirve para seleccionar una celda de la Table View de empleados
     */
    public Empleado getTableEmpleadoSeleccionado() {

        tInforme.setVisible(true);

        if (tvEmpleados != null) {
            Empleado empleado = tvEmpleados.getSelectionModel().getSelectedItem();
            return empleado;
        }
        return null;
    }

    /**
     * Este método sirve para seleccionar una celda de la Table View de proyectos
     */
    public Proyecto getTableProyectoSeleccionado() {

        tInforme.setVisible(true);

        if (tvProyectos != null) {
            Proyecto proyecto = tvProyectos.getSelectionModel().getSelectedItem();
            return proyecto;
        }
        return null;
    }

    /**
     * Este método sirve para seleccionar una celda de la Table View de participan
     */
    public Participan getTableParticipanSeleccionado() {

        tInforme.setVisible(true);

        if (tvParticipan != null) {
            Participan participan = tvParticipan.getSelectionModel().getSelectedItem();
            this.participan = participan;
            return participan;
        }
        return null;
    }

    //TODO De aquí para abajo es el código del panel derecho de "Incurrir Horas" (Participan)

    @FXML
    public TextField tfHorasNombreEmpleado;
    @FXML
    public TextField tfHorasNombreProyecto;
    @FXML
    public TextField tfHoras;

    @FXML
    Button btIncurrir;
    @FXML
    Button btModificar;
    @FXML
    Button btBorrar;

    public Participan participan;

    /**
     * Método para gestionar los posibles errores que se puedan dar en el programa
     */
    //TODO Si decides optimizar o añadir cosas a este método, lleva mucho cuidado porque es MUY complejo
    private void gestionDeErrores(String boton, boolean tabEmpleadosSelected) throws NumberFormatException {

        eliminaErrores();
        boolean numeroValido = false;

        if (tabEmpleadosSelected) {

            if (boton.equals("actualizar") || boton.equals("crear")) {

                boolean telfValido = Regex.compruebaTelefono(tfTelfNewEmpleado.getText());
                boolean nombreValido = Regex.compruebaNombres(tfNombreNewEmpleado.getText());

                if (!nombreValido) {
                    try {
                        throw new NombreNoValido();
                    } catch (NombreNoValido e) {
                        warning = true;
                        mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                } else if (!telfValido) {
                    try {
                        throw new TelefonoNoValido();
                    } catch (TelefonoNoValido e) {
                        warning = true;
                        mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                } else if (tfNombreNewEmpleado.getText().trim().isEmpty()) {
                    try {
                        throw new FaltanDatos(tfNombreNewEmpleado);
                    } catch (FaltanDatos e) {
                        warning = true;
                        mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                }
            }
        } else {
            if (boton.equals("actualizar") || boton.equals("crear")) {

                boolean nombreValido = Regex.compruebaNombres(tfNombreNewProject.getText());

                if (!nombreValido) {
                    try {
                        throw new NombreNoValido();
                    } catch (NombreNoValido e) {
                        warning = true;
                        mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                } else if (tfNombreNewProject.getText().trim().isEmpty()) {
                    try {
                        throw new FaltanDatos();
                    } catch (FaltanDatos e) {
                        warning = true;
                        mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                } else if (taDescripNewProject.getText().trim().isEmpty()) {
                    try {
                        throw new FaltanDatos(taDescripNewProject);
                    } catch (FaltanDatos e) {
                        warning = true;
                        mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                }
            }
        }

        if (boton.equals("modificar") || boton.equals("incurrir")) {

            numeroValido = Regex.compruebaHoras(tfHoras.getText());

            if (!numeroValido) {
                try {
                    throw new HorasNoValidas(tfHoras);
                } catch (HorasNoValidas e) {
                    warning = true;
                    mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                }
            }

            if (!warning) {
                if (Float.parseFloat(tfHoras.getText()) > 100f) {
                    try {
                        throw new HorasNoValidas("100");
                    } catch (HorasNoValidas e) {
                        warning = true;
                        mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                }
            }

            if (!warning) {
                switch (boton) {

                    case "incurrir":

                        if (tfHoras.getText().equals("") && tfHorasNombreProyecto.getText().equals("") && tfHorasNombreEmpleado.getText().equals("")) {
                            try {
                                throw new FaltanDatos("", "");
                            } catch (FaltanDatos e) {
                                warning = true;
                                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                            }
                        } else if (tfHoras.getText().equals("0")) {
                            try {
                                throw new HorasNoValidas(0);
                            } catch (HorasNoValidas e) {
                                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                                warning = true;
                            }
                        } else if (!numeroValido || tfHoras.getText().equals("")) {
                            try {
                                if (tfHoras.getText().equals("")) {
                                    throw new HorasNoValidas();
                                } else
                                    throw new HorasNoValidas(tfHoras);
                            } catch (HorasNoValidas e) {
                                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                                warning = true;
                            }
                        } else if (tfHorasNombreEmpleado.getText().equals("") || tfHorasNombreProyecto.getText().equals("")) {
                            try {
                                if (tfHorasNombreEmpleado.getText().equals("") && tfHorasNombreProyecto.getText().equals("")) {
                                    throw new FaltanDatos(tfHorasNombreEmpleado, tfHorasNombreProyecto);
                                } else if (tfHorasNombreEmpleado.getText().equals("")) {
                                    throw new FaltanDatos(tfHorasNombreEmpleado);
                                } else
                                    throw new FaltanDatos();
                            } catch (FaltanDatos e) {
                                warning = true;
                                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                            }
                        }
                        break;

                    case "modificar":

                        if (this.participan == null) {
                            try {
                                throw new FaltanDatos("");
                            } catch (FaltanDatos e) {
                                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                                warning = true;
                            }
                        } else if (this.participan.getEmpleado().getNombreempleado().equals(tfHorasNombreEmpleado.getText()) &&
                                this.participan.getProyecto().getNombre().equals(tfHorasNombreProyecto.getText()) &&
                                this.participan.getHoras() == Float.parseFloat(tfHoras.getText())) {
                            try {
                                throw new UpdateSinCambios();
                            } catch (UpdateSinCambios e) {
                                warning = true;
                                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                            }
                        }
                        break;
                }
            }
        }
    }


    @FXML
    public void onIncurrirClick() throws IOException {

        informacion.delete(0, informacion.length());
        tInforme.setText(informacion.toString());

        try {
            gestionDeErrores("incurrir", false);
        } catch (NumberFormatException e) {
            System.err.println("Error gestionado: " + e);
            try {
                if (tfHoras.getText().equals("")) {
                    throw new HorasNoValidas();
                } else
                    throw new HorasNoValidas("100");
            } catch (HorasNoValidas ex) {
                warning = true;
                mostrarInfoEfimera("warning", ex, null, InitialApplication.stageInitial.getScene().getWidth());
            }
        }

        if (!warning && !error) {
            Consultas.createParticipan(empleadoSeleccionado.getEmpleadoid(),
                    proyectoSeleccionado.getProyectoid(),
                    tfHoras.getText());

            Participan participanNuevo = new Participan();
            participanNuevo.setEmpleadoid(empleadoSeleccionado.getEmpleadoid());
            participanNuevo.setProyectoid(proyectoSeleccionado.getProyectoid());
            participanNuevo.setHoras(Float.parseFloat(tfHoras.getText()));

            this.participan = participanNuevo;

            InitialApplication.abrirStageInicial(new Stage(), "create", null, String.valueOf(participanNuevo.getClass()), true);
        }
        //Después del proceso volvemos a asignar a las variables de incidencias su valor original
        eliminaErrores();
    }

    @FXML
    public TabPane tabPane;

    @FXML
    public void onModificarParticipan() throws IOException {

        informacion.delete(0, informacion.length());
        tInforme.setText(informacion.toString());

        try {
            gestionDeErrores("modificar", false);
        } catch (NumberFormatException e) {
            System.err.println("Error gestionado: " + e);
            warning = true;
            mostrarInfoEfimera("modificar", e, null, InitialApplication.stageInitial.getScene().getWidth());
        }

        if (!warning && !error) {
            //Como en el campo solo aparece el nombre, se realizan búsquedas personalizadas filtrando por nombre para
            // obtener el empleado y el proyecto
            String busquedaEmpleado = BridgeAPI.conection(Consultas.findByNombreEmpleado(tfHorasNombreEmpleado.getText()));
            JSONArray jsonArrayEmpleado = new JSONArray(busquedaEmpleado);
            Empleado empleado = CreaObjetos.creaEmpleados(jsonArrayEmpleado.getJSONObject(0), null);

            String busquedaProyecto = BridgeAPI.conection(Consultas.findByNombreProyecto(tfHorasNombreProyecto.getText()));
            JSONArray jsonArrayProyecto = new JSONArray(busquedaProyecto);
            Proyecto proyecto = CreaObjetos.creaProyectos(jsonArrayProyecto.getJSONObject(0), null);

            //Y se saca el id de estos dos objetos para pasarlos como parámetro al método updateParticipan() y así poder
            // updatear el registro de Participan
            Consultas.updateParticipan(this.participan, empleado.getEmpleadoid().toString(), proyecto.getProyectoid().toString(), tfHoras.getText());

            InitialApplication.abrirStageInicial(new Stage(), "update", null, participan.getClass().toString(), true);
        }
        //Después del proceso volvemos a asignar a las variables de incidencias su valor original
        eliminaErrores();
    }

    @FXML
    public void onBorrarParticipan() throws IOException {

        informacion.delete(0, informacion.length());
        tInforme.setText(informacion.toString());

        if (this.participan != null) {
            InitialApplication.stageConfirmacion = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(InitialApplication.class.getResource("confirmacion-usuario-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 464, 184);

            ConfirmationController confirmationController = (ConfirmationController) fxmlLoader.getController();
            confirmationController.recibeParticipan(this.participan);

            InitialApplication.stageConfirmacion.setTitle("Confirmación de eliminación del registro de un proyecto");
            InitialApplication.stageConfirmacion.setScene(scene);
            InitialApplication.stageConfirmacion.show();
        } else {
            try {
                throw new FaltanDatos("");
            } catch (FaltanDatos e) {
                warning = true;
                mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
            }
        }
    }

    @FXML
    public TextField tfIdEmpleado;
    @FXML
    public TextField tfTelfNewEmpleado;
    @FXML
    public TextField tfNombreNewEmpleado;
    @FXML
    public Tab tabEmpleados;

    @FXML
    public ChoiceBox<Empleado> cbEmpleados;

    public Empleado empleado;

    public static ObservableList<Empleado> listadoEmpleados;

    private boolean mismoTelefono = false;


    /**
     * Este método carga los datos del ChoiceBox cada vez que se añade, modifica o se elimina un empleado
     */
    public ObservableList<Empleado> obtenerEmpleados() {

        ObservableList<String> listNombresEmpleados = FXCollections.observableArrayList();
        ObservableList<Empleado> listEmpleados = FXCollections.observableArrayList();
        String resultado = BridgeAPI.conection(Consultas.CONSULTA_LISTAR_EMPLEADOS);

        JSONArray jsonArray = new JSONArray(resultado);
        //Bucle para generar un objeto proyecto para cada objeto que nos devuelve la query
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject item = jsonArray.getJSONObject(i);

            Empleado empleado = CreaObjetos.creaEmpleados(item, null);

            listNombresEmpleados.add(empleado.getNombreempleado());
            listEmpleados.add(empleado);
        }

        cbEmpleados.setItems(listEmpleados);
        listadoEmpleados = listEmpleados;

        return listEmpleados;
    }

    /**
     * A este método se le llama cuando necesitamos mostrar 1 empleado en concreto por pantalla
     */
    public void mostrarEmpleado(ObservableList<Empleado> listadoEmpleados) {

        if (this.empleado == null) {
            this.empleado = listadoEmpleados.get(0);
        }

        for (Empleado unEmpleado : listadoEmpleados) {
            if (this.empleado.getEmpleadoid().equals(unEmpleado.getEmpleadoid())) {
                cbEmpleados.setValue(unEmpleado);

                tfNombreNewEmpleado.setText(unEmpleado.getNombreempleado());
                tfTelfNewEmpleado.setText(unEmpleado.getTelefono());
                tfIdEmpleado.setText(String.valueOf(unEmpleado.getEmpleadoid()));
                break;
            }
        }
    }

    /**
     * Este método ajusta el tamaño del tInfo en base al tamaño total de la pantalla en la que se ejecute este programa y el tamaño de la ventana
     */
    //TODO Este método es bastante complejo así que no borres NINGÚN comentario porque te podrían hacer falta
    public void ajustaTextoAPantalla(double anchoDelPane) {

        //(alto == 1260 && ancho == 2240) ---> Tamaño de pantalla de mi iMac (Creo que es el mismo que el tamaño max del Stage)

        //REGLA DE 3 BASE PARA REALIZAR LOS CALCULOS DE LA POSICION OPTIMA PARA EL OBJETO TEXT
        //                   %         Px
        //                  100; --- 2200
        //                     x ---  900
        //Aclaraciones:
        //      ( 2200 = Tamaño máximo del Pane que contiene el Text tInfo)
        //      ( 900 = Es la posición óptima para que quede centrado el Text tInfo con el tamaño máximo del Pane)

        // Fómula = (100x900)/2200 = x = Al tanto por cien necesario para sacar la posición ideal del resto de tamaños

        //Una vez que hemos obtenido el valor de x, ya podemos usarlo para sacar la 2a Regla de 3 que nos dará lo que buscamos

        //REGLA DE 3 SECUNDARIA PARA REALIZAR LOS CÁLCULOS DE LA POSICIÓN ÓPTIMA RELATIVA PARA EL OBJETO TEXT
        //                   %         Px
        //                  100 --- tamañoActualPane
        //                    x --- y
        // y = (tamanyoActualPane * x) / 100

//        System.out.println("Tamaño actual del Pane: " + pBottom.getWidth());

        //900 es la posición óptima a pantalla completa, realizo los cálculos basándome en esto porque sé que queda bien.
        double x = (900 * 100) / TAMANYO_MAX_PANE_BOTTOM;
        double posicionCentrada = (anchoDelPane * x) / 100;
        //Si se pudiera hacer muy pequeña la ventana (Que no es el caso), podríamos modificar la escala de la letra de esta manera:
        //        tInfo.setScaleX();

//        System.out.println("% De la posición original: " + posicionCentrada);

//Nunca va a entrar aquí porque he ajustado el tamaño mínimo del tamaño Stage, pero lo dejo por si me hiciera falta el método de dentro
//        if (pBottom.getWidth() < 500) {
//            posicionCentrada -= 130;
//
//            //TODO Hago que el tamaño horizontal asignado a este objeto Text sea más pequeño que el inicial que le puse
//            tInfo.setWrappingWidth(tInfo.getWrappingWidth() - 100);
//        }

        //Seguro que hay formas mejores de hacer esto, pero así se queda. Modifico un poco los valores para que queden bien ajustados
        if (pBottom.getWidth() < 600)
            posicionCentrada -= 175;
        else if (pBottom.getWidth() < 800)
            posicionCentrada -= 150;
        else if (pBottom.getWidth() < 1600)
            posicionCentrada -= 100;

//        System.out.println("% De la posición modificada en el paso anterior: " + posicionCentrada);

        tInfo.setLayoutX(posicionCentrada - 30); //ancho
        tInfo.setLayoutY(100); //alto 100, aunque no es necesario siempre es el mismo

        //TODO NO BORRAR. Es para mostrar por pantalla la información relativa al tamaño de la pantalla y los objetos para ajustarla
//        if (InitialApplication.stageInitial.getScene() != null) {
//            double altoScene = InitialApplication.stageInitial.getScene().getHeight();
//            double anchoScene = InitialApplication.stageInitial.getScene().getWidth();
//
//            //De esta manera podemos comprobar los tamaños que me interesan para ajustar el tamaño del Text tInfo al de su objeto padre
//            System.out.println("Alto Scene- Y - Heigth: " + altoScene);
//            System.out.println("Ancho Scene- X - Width: " + anchoScene);
//
//            System.out.println("Alto Scene Window- Y - Heigth: " + InitialApplication.stageInitial.getScene().getWindow().getY());
//            System.out.println("Ancho Scene Window- X - Width: " + InitialApplication.stageInitial.getScene().getWindow().getX());
//            //Objeto padre del Text tInfo
//            System.out.println("Alto Pane Bottom- Y - Heigth: " + pBottom.getHeight());
//            System.out.println("Ancho Pane Bottom- X - Width: " + pBottom.getWidth());
//
//            //TODO Este método nos dice si el objeto padre (el Pane) puede redimensionar al objeto que lo llama
////        System.out.println("Se puede redimensionar un objeto Text?: " + tInfo.isResizable());
//        }
    }

    /**
     * Este método comprueba que no se superen el tamaño de los campos predefinido en la BD
     */
    public void compruebaLimites(TextInputControl ticNombre, TextInputControl ticDescrip) {

        //Límites de cada uno en la BD:     50           300                  25
        //Posibles entradas: tfNombreNewProject, taDescripNewProject, tfNombreNewEmpleado};
        //El teléfono del empleado no es necesario añadirlo pues la REGEX ya lo comprueba

        eliminaErrores();

        int limiteMax = 0;

        if (ticDescrip == null) {
            limiteMax = 25;
            if (ticNombre.getText().length() > limiteMax) {
                try {
                    throw new SuperadoLimiteDeBd(tfNombreNewEmpleado);
                } catch (SuperadoLimiteDeBd e) {
                    error = true;
                    mostrarInfoEfimera("error", e, null, InitialApplication.stageInitial.getScene().getWidth());
                }
            }
        } else {
            limiteMax = 50;
            if (ticNombre.getText().length() > limiteMax) {
                try {
                    throw new SuperadoLimiteDeBd(tfNombreNewProject, taDescripNewProject);
                } catch (SuperadoLimiteDeBd e) {
                    error = true;
                    mostrarInfoEfimera("error", e, null, InitialApplication.stageInitial.getScene().getWidth());
                }
            }
            if (!error) {
                limiteMax = 300;
                if (ticDescrip.getText().length() > limiteMax) {
                    try {
                        throw new SuperadoLimiteDeBd(taDescripNewProject);
                    } catch (SuperadoLimiteDeBd e) {
                        error = true;
                        mostrarInfoEfimera("error", e, null, InitialApplication.stageInitial.getScene().getWidth());
                    }
                }
            }
        }
    }

    /**
     * Este método limpia los boolean asociados a cualquier error, aviso o tipo de warning
     */
    public void eliminaErrores() {

        this.error = false;
        this.mismoNombre = false;
        this.mismoTelefono = false;
        this.warning = false;
    }

//TODO A partir de aquí están las funcionalidades de los elementos del MenúBar (La barra de arriba donde pone file, edit, help...)

    @FXML
    public void onClose() {
        InitialApplication.stageInitial.close();
        if (InitialApplication.stageHelpAbout != null) {
            InitialApplication.stageHelpAbout.close();
        }
        if (InitialApplication.stageFondos != null) {
            InitialApplication.stageFondos.close();
        }
    }

    @FXML
    public void onAbout() throws IOException {

        InitialApplication.stageHelpAbout = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(InitialApplication.class.getResource("help-about-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 600);
        //Hago que no se pueda modificar el tamaño de este stage
        InitialApplication.stageHelpAbout.setResizable(false);
        //Y lo inicio en la posición que a mí me interesa
        InitialApplication.stageHelpAbout.setY(200);
        InitialApplication.stageHelpAbout.setX(200);

        //El stage es el escenario, el decir el marco donde se ejecuta la app (Con sus botones de maximizar, minimizar...etc)
        InitialApplication.stageHelpAbout.setTitle("Sección de Ayuda");
        //La Scene es la escena, que está dentro del Stage (escenario). Es como en las obras de teatro.
        InitialApplication.stageHelpAbout.setScene(scene);
        InitialApplication.stageHelpAbout.show();
    }

    @FXML
    public void onFondos() throws IOException {

        InitialApplication.stageFondos = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(InitialApplication.class.getResource("fondos-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 400, 400);
        //Hago que no se pueda modificar el tamaño de este stage
        InitialApplication.stageFondos.setResizable(false);
        //Y lo inicio en la posición que a mí me interesa
        InitialApplication.stageFondos.setY(200);
        InitialApplication.stageFondos.setX(700);

        //El stage es el escenario, el decir el marco donde se ejecuta la app (Con sus botones de maximizar, minimizar...etc)
        InitialApplication.stageFondos.setTitle("Panel de personalización");
        //La Scene es la escena, que está dentro del Stage (escenario). Es como en las obras de teatro.
        InitialApplication.stageFondos.setScene(scene);
        InitialApplication.stageFondos.show();
    }

    //TODO Personalización de la Aplicación

    @FXML
    public SplitMenuButton smbColores;
    @FXML
    public CheckBox checkBoxLetras;

    public static boolean checkBoxActivado = false;

    private final String[] colores = new String[]{"blanco","negro", "azul", "lila", "celeste"};
    public static String colorElegido;

    /**
     * Este método devuelve objetos MenuItem para ir asignándolos al SplitMenuButtom
     */
    private MenuItem devuelveMenuItems() {

        return new MenuItem();
    }

    /**
     * Este método crea cada elemento del SplitMenuButton y les asigna un texto
     */
    private void creaMenuItem() {

        MenuItem menuItem1 = devuelveMenuItems();
        menuItem1.setOnAction((e) -> {
            smbColores.setText(colores[0]);
            colorElegido = colores[0];
        });
        menuItem1.setText(colores[0]);

        MenuItem menuItem2 = devuelveMenuItems();
        menuItem2.setOnAction((e) -> {
            smbColores.setText(colores[1]);
            colorElegido = colores[1];
        });
        menuItem2.setText(colores[1]);

        MenuItem menuItem3 = devuelveMenuItems();
        menuItem3.setOnAction((e) -> {
            smbColores.setText(colores[2]);
            colorElegido = colores[2];
        });
        menuItem3.setText(colores[2]);

        MenuItem menuItem4 = devuelveMenuItems();
        menuItem4.setOnAction((e) -> {
            smbColores.setText(colores[3]);
            colorElegido = colores[3];
        });
        menuItem4.setText(colores[3]);

        MenuItem menuItem5 = devuelveMenuItems();
        menuItem5.setOnAction((e) -> {
            smbColores.setText(colores[4]);
            colorElegido = colores[4];
        });
        menuItem5.setText(colores[4]);

        //Modifico todos los menuItems iniciales y añado los creados al objeto SplitMenuButtom
        smbColores.getItems().setAll(menuItem1, menuItem2, menuItem3, menuItem4, menuItem5);
    }

    @FXML
    /**Al seleccionar el SplitMenuButtom, nos cambiará los colores que queramos de la aplicación*/
    public void onSMBClick(){

        //Si no selecciona ninguno y devuelve la palabra "Colores" saltará un aviso
        switch (smbColores.getText()){
            case "Colores":
                try {
                    throw new ColorNoSeleccionado();
                } catch (ColorNoSeleccionado e) {
                    warning = true;
                    informacion.delete(0, informacion.length());
                    tInforme.setText(informacion.toString());
                    mostrarInfoEfimera("warning", e, null, InitialApplication.stageInitial.getScene().getWidth());
                }
                break;
            case "blanco":

                Paint blanco = creaPaint("0xffffff");

                if (!checkBoxLetras.isSelected()) {
                    InitialApplication.colorPane = blanco;
                    cambiarColorPane(blanco);
                }else{
                    InitialApplication.colorLetras = blanco;
                    cambiarColorLetras(blanco);
                }
                break;
            case "negro":

                Paint negro = creaPaint("0x000000");

                if (!checkBoxLetras.isSelected()) {
                    InitialApplication.colorPane = negro;
                    cambiarColorPane(negro);
                }else{
                    InitialApplication.colorLetras = negro;
                    cambiarColorLetras(negro);
                }
                break;
            case "azul":

                Paint azul = creaPaint("0x0009ff");

                if (!checkBoxLetras.isSelected()) {
                    InitialApplication.colorPane = azul;
                    cambiarColorPane(azul);
                }else{
                    InitialApplication.colorLetras = azul;
                    cambiarColorLetras(azul);
                }
                break;
            case "lila":

                Paint lila = creaPaint("0xbf00ff");

                if (!checkBoxLetras.isSelected()) {
                    InitialApplication.colorPane = lila;
                    cambiarColorPane(lila);
                }else{
                    InitialApplication.colorLetras = lila;
                    cambiarColorLetras(lila);
                }
                break;
            case "celeste":

                Paint celeste = creaPaint("0x00a4ff");

                if (!checkBoxLetras.isSelected()) {
                    InitialApplication.colorPane = celeste;
                    cambiarColorPane(celeste);
                }else{
                    InitialApplication.colorLetras = celeste;
                    cambiarColorLetras(celeste);
                }
                break;
        }
    }

    @FXML public Text tID;
    @FXML public Text tNombreE;
    @FXML public Text tTelefono;
    @FXML public Text tPa;
    @FXML public Text tSeccionHoras;
    @FXML public Text tEmpleado;
    @FXML public Text tProyecto;
    @FXML public Text tHoras;

    @FXML public Text tIdP;
    @FXML public Text tNombreP;
    @FXML public Text tDescrip;

    @FXML public Text tEmpleadosT;
    @FXML public Text tProyectosT;

    private ArrayList<Text> textos;

    /**Este método cambia el color de todas las letras, es decir de todos los objetos Text*/
    public void cambiarColorLetras(Paint color){

        textos = new ArrayList<>() {{add(tID); add(tNombreE); add(tTelefono); add(tPa); add(tSeccionHoras); add(tEmpleado);
            add(tProyecto); add(tHoras); add(tIdP); add(tNombreP); add(tDescrip); add(tEmpleadosT); add(tProyectosT); add(tInforme);}};

        checkBoxLetras.setTextFill(color);

        for (Text texto : textos) {
            texto.setFill(color);
        }
    }

    /**Este método cambia el color del Pane que muestra la información*/
    public void cambiarColorPane(Paint color){
        //De esta forma cambiamos el color del fondo a un Pane
        pBottom.setBackground(Background.fill(color));
    }

    @FXML
    public void onCheckBoxClick(){

        if (this.checkBoxLetras.isSelected()) {
            checkBoxActivado = true;
            InitialApplication.checkBoxActivado = true;
        }else {
            checkBoxActivado = false;
            InitialApplication.checkBoxActivado = false;
        }
    }

    public Paint creaPaint(String color){
        return Paint.valueOf(color);
    }


    /**Lee el fichero que le llegue como parámetro, mete los valores en un Diccionario y lo devuelve
     * @param nombreArchivo el cual queramos leer
     * @return un Map<String, String> con el contenido obtenido del archivo
     * @author José Sánchez García</autor>
     * */
    public Map<String, String> obtenerDatos(String nombreArchivo) throws IOException {

        Map<String, String> hashMap = new HashMap<>();
        File file = new File("./src/main/resources/" + nombreArchivo);

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            String line = br.readLine();

            while (line != null) {
                hashMap.put(line.split(" ")[0], line.split(" ")[1]);
                line = br.readLine();
            }
        }
        return hashMap;
    }
}


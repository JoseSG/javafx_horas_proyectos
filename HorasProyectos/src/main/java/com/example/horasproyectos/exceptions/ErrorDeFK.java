package com.example.horasproyectos.exceptions;

public class ErrorDeFK extends Exception {

    public ErrorDeFK() {
        super("Esa acción no se puede efectuar.\nDebe eliminar los registros de horas asociados");
    }
}

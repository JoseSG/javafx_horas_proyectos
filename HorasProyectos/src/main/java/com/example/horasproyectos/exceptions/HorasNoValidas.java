package com.example.horasproyectos.exceptions;

import javafx.scene.control.TextField;

public class HorasNoValidas  extends Exception{

    public HorasNoValidas(TextField tfHoras){
        super("Horas introducidas no válidas.\nPor favor, introduzca un valor entero o con un solo decimal usando \".\" el valor decimal debe ser 5");
    }
    public HorasNoValidas(){
        super("Campo de horas incurridas vacío.\nPor favor, introduzca un valor entero o con decimales utilizando \".\"");
    }
    public HorasNoValidas(Integer zero){
        super("Este campo no puede estar a 0.\nPor favor, introduzcas un valor entero o con decimales utilizando \".\"");
    }
    public HorasNoValidas(String limiteDeHoras){
        super("No se pueden introducir más de 100 horas en un registro.\nPor favor, introduzca un número menor.");
    }
}
package com.example.horasproyectos;

import com.example.horasproyectos.controllers.FondosController;
import com.example.horasproyectos.controllers.InitialController;
import com.example.horasproyectos.models.*;
import com.example.horasproyectos.utilities.Encriptacion;
import javafx.application.Application;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

import javafx.scene.image.ImageView;

import java.awt.*;
import java.io.File;
import java.io.IOException;

//TODO: Nota
// Este proyecto solamente guarda de configuración personalizada el fondo escogido. Se podría mejorar para guardar
// el color de la fuente escogido y el tamaño de la ventana ajustada por el usuario


public class InitialApplication extends Application {

    public static Stage stageInitial;
    public static Stage stageConfirmacion;
    public static Stage stageHelpAbout;
    public static Stage stageFondos;

    public static ImageView fondo;
    public static int idFondo;
    //appActualizada/refrescar implica que se ha reiniciado y vuelto a abrir el programa para actualizar el contenido de las tablas,
    // básicamente he creado esta variable para saber cuándo cargar los mensajes informativos para el usuario antes de abrir el stage
    public static boolean refrescar = false;

    public static double anchoPantalla, altoPantalla;

    public static double altoVentana = 800;
    public static double anchoVentana = 600;

    public static double posicionXScene = 0.0, posicionYScene = 0.0;

    public static boolean checkBoxActivado = false;
    //Por defecto, el color del Pane de mostrar información será negro.
    public static Paint colorPane = Paint.valueOf("0x000000");
    public static Paint colorLetras;

    public static boolean urlImagenesUoVacio = true;

    public static final String URL_CONFIGURACION = "./src/main/resources/com/example/horasproyectos/copias/configuracion.txt";

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {

        obtenerTamanyoPantalla();
        abrirStageInicial(stage, null, null, null, false);

    }

//    TODO Falla este método porque no guarda las rutas de las nuevas imágenes
    @Override
    //Añadimos aquí lo que queramos hacer antes de cerrar el programa, por ej guardar la configuración elegida por el usuario
    public void stop() {
        //Guardo las rutas de los archivos encriptados que se corresponden con los fondos elegidos por el uo antes de que se cierre
        // el programa y las meto en un archivo llamado configuracion.txt

        //Primero guardamos lo que contuviera el archivo configuracion.txt anterior
        String linea = FondosController.leerLineaDeArchivo();
        //Si ha cambiado el fondo lo actualizamos en el archivo configuracion.txt
        if (FondosController.cambioDeFondo) {
            linea = linea.replaceFirst("\\d", String.valueOf(idFondo));
        }
        //TODO: En esta linea está la generación del archivo configuracion.txt en el que se guarda la información sobre
        // la configuración que ha elegido el usuario (solo se guardan datos referentes a los fondos)
        File configuracion = new File(URL_CONFIGURACION);
        StringBuilder urlImagenes = new StringBuilder();
        urlImagenes.append(idFondo + "#");

        String caracter = "";
        String cadena = "";

        //Automatizo el proceso de generar el texto que contendará configuracion.txt
        for (int i = 0; i < FondosController.urlImagenesUo.length; i++) {

            cadena = FondosController.urlImagenesUo[i];
            if (cadena != null) {
                urlImagenes.append(caracter + cadena);
                caracter = (caracter.equals("")) ? "$" : "%";
            }
        }

        byte[] fileBytes = null;
        //Si no se ha arrastrado ninguna imagen nueva, se mantendrán las que hubiera en el archivo anterior
        if (!urlImagenesUoVacio) {
            fileBytes = urlImagenes.toString().getBytes();
        } else {
            fileBytes = linea.getBytes();
        }
        Encriptacion.generaCopiaEscrita(fileBytes, configuracion);
        System.exit(0);
    }


    /**
     * Este método abre la stage inicial (la principal) de la aplicación
     */
    public static void abrirStageInicial(Stage stage, String proceso, Exception exception, String clase,
                                         boolean actualizarApp) throws IOException {

        //Si es la primera vez que se abre el programa, se cargará el fondo último escogido, cuyo id está en el archivo de configuración
        if (!actualizarApp) {
            cargarFondoEscogido();
        }

        refrescar = actualizarApp;

        if (InitialApplication.stageInitial != null) {
            if (InitialApplication.stageInitial.isShowing()) {
                //Me quedo con la posición de la escena antes de cerrarla
                posicionXScene = InitialApplication.stageInitial.getScene().getWindow().getX();
                posicionYScene = InitialApplication.stageInitial.getScene().getWindow().getY();
                //Y con el tamaño de la ventana por si la ha modificado el usuario
                anchoVentana = InitialApplication.stageInitial.getScene().getHeight();
                altoVentana = InitialApplication.stageInitial.getScene().getWidth();

                //Si había una pantalla principal la cierro para luego volverla a abrir y así refrescar los datos de las
                // modificaciones en la BD
                InitialApplication.stageInitial.close();
            }
        }
        InitialApplication.stageInitial = stage;

        //Le asigno al stage la posición inicial y la que le asigne el usuario al mover la ventana
        InitialApplication.stageInitial.setY(posicionYScene);
        InitialApplication.stageInitial.setX(posicionXScene);

        FXMLLoader fxmlLoader = new FXMLLoader(InitialApplication.class.getResource("initial-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), altoVentana, anchoVentana);

        //Creo una instancia de la primera pantalla para cargar datos en sus elementos con antelación
        InitialController initialController = (InitialController) fxmlLoader.getController();
        ObservableList<Proyecto> listProyectos = initialController.obtenerProyectos();
        ObservableList<Empleado> listEmpleados = initialController.obtenerEmpleados();
        initialController.mostrarProyecto(listProyectos);
        initialController.mostrarEmpleado(listEmpleados);

        if (colorLetras != null) {
            initialController.cambiarColorLetras(colorLetras);
        }
        if (colorPane != null) {
            initialController.cambiarColorPane(colorPane);
        }
        if (checkBoxActivado) {
            initialController.checkBoxLetras.setSelected(true);
        }
        if (refrescar) {
            initialController.mostrarInfoEfimera(proceso, exception, clase, scene.getWidth());
        }

        //De esta manera impido que el usuario pueda hacer muy pequeño el Stage o escenario, es la ventana principal
        stageInitial.setMinWidth(565);
        stageInitial.setMinHeight(625);
        //También impido que el stage se pueda hacer más grande que la pantalla que lo contiene
        stageInitial.setMaxWidth(altoPantalla);
        stageInitial.setMaxHeight(anchoPantalla);

        refrescar = false;
        //El stage es el escenario, el decir el marco donde se ejecuta la app (Con sus botones de maximizar, minimizar...etc)
        stageInitial.setTitle("Horas Proyectos");
        //La Scene es la escena, que está dentro del Stage (escenario). Es como en las obras de teatro.
        stageInitial.setScene(scene);
        stageInitial.show();
    }

    /**
     * Este método indica qué tamaño tiene la pantalla del usuario para luego ajustar el tamaño del tInfo en base a
     * esto para que siempre se muestre centrado
     */
    private void obtenerTamanyoPantalla() {

        Dimension tamanyoPantalla = Toolkit.getDefaultToolkit().getScreenSize();
        altoPantalla = tamanyoPantalla.getWidth();
        anchoPantalla = tamanyoPantalla.getHeight();

//        De esta manera siempre me abre la app a pantalla completa, aunque ya no me interesa
//        anchoVentana = anchoPantalla;
//        altoVentana = altoPantalla;
//        Nota: Con este método obtendríamos lo mismo pero sin saber las medidas de la pantalla del usuario
//        stageInitial.setMaximized(true);
    }

    /**
     * Este método carga la última imagen de fondo que escogió el usuario
     */
    public static void cargarFondoEscogido() throws IOException {
        String linea = FondosController.leerLineaDeArchivo();
        String idFondoGuardado = linea.substring(0, linea.indexOf("#"));
        switch (idFondoGuardado) {
            case "1":
                fondo = new ImageView();
                fondo.setImage(new Image(new File("./src/main/resources/com/example/horasproyectos/fondo-colorines.jpg").toURI().toString()));
                break;
            case "2":
                fondo = new ImageView();
                fondo.setImage(Encriptacion.decodificar(new File("./src/main/resources/com/example/horasproyectos/copias/2.txt")));
                break;
            case "3":
                fondo = new ImageView();
                fondo.setImage(new Image(new File("./src/main/resources/com/example/horasproyectos/fondo-eclipse-espacio.jpg").toURI().toString()));
                break;
            case "4":
                fondo = new ImageView();
                fondo.setImage(Encriptacion.decodificar(new File("./src/main/resources/com/example/horasproyectos/copias/4.txt")));
                break;
            case "5":
                fondo = new ImageView();
                fondo.setImage(new Image(new File("./src/main/resources/com/example/horasproyectos/fondo-nota-colorida.jpg").toURI().toString()));
                break;
            case "6":
                fondo = new ImageView();
                fondo.setImage(Encriptacion.decodificar(new File("./src/main/resources/com/example/horasproyectos/copias/6.txt")));
                break;
        }
    }
}
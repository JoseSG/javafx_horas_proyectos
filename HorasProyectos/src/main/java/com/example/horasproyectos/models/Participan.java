package com.example.horasproyectos.models;

import java.util.Objects;

public class Participan {

    private Integer registroid;
    private Integer empleadoid;
    private Integer proyectoid;
    private float horas;

    private Empleado empleado;
    private Proyecto proyecto;

    public Participan() {
    }

    public Integer getRegistroid() {
        return registroid;
    }

    public void setRegistroid(Integer registroid) {
        this.registroid = registroid;
    }

    public Integer getEmpleadoid() {
        return empleadoid;
    }

    public void setEmpleadoid(Integer empleadoid) {
        this.empleadoid = empleadoid;
    }

    public Integer getProyectoid() {
        return proyectoid;
    }

    public void setProyectoid(Integer proyectoid) {
        this.proyectoid = proyectoid;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Participan)) return false;
        Participan that = (Participan) o;
        return registroid.equals(that.registroid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registroid);
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
}

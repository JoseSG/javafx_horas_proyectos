package com.example.horasproyectos.utilities;

/**El nombre de esta clase viene de REGular EXpresion, aquí estarán todas las REGEX que me utilizo en el proyecto*/
public class Regex {

    //Comprueba si lo que le llega es un número positivo entero o con 1 decimal separado por un . solo admite el 5 como
    // valor decimal
    private static String regexHoras = "[+]?\\d+(\\.[5]{1})?"; //Ejemplos válidos: 3, 1.5, 4.5, 2, 0.5 ...
    //Comprueba si lo que le llega es un número de teléfono válido (un número positivo de 9 dígitos)
    private static String regexTelf = "[+]?(\\d{9})+";
    //Comprueba que se recibe un nombre en minúsculas, que contenga mínimo 3 letras, para que siempre funcione pásale la
    // cadena .tolowercase() y ya, así no te complicas tanto
    private static String regexNombre = "([a-z]{3,})+";

    /**
     * Método creado para comprobar si se introduce un valor válido en el campo de horas
     */
    public static boolean compruebaHoras(String horasIntroducidas) {

        String textoEvaluado = horasIntroducidas.toLowerCase();
        boolean numeroValido = textoEvaluado.matches(regexHoras);

        return numeroValido;
    }

    /**
     * Método creado para comprobar si se introduce un valor válido en el campo de teléfono
     */
    public static boolean compruebaTelefono(String telfIntroducido) {

        String telfEvaluado = telfIntroducido.toLowerCase();
        boolean telfValido = telfEvaluado.matches(regexTelf);

        return telfValido;
    }

    /**
     * Método creado para comprobar si se introduce un valor válido en el campo de nombre
     */
    public static boolean compruebaNombres(String nombreIntroducido) {

        String nombreEvaluado = nombreIntroducido.toLowerCase();
        boolean nombreValido = nombreEvaluado.matches(regexNombre);

        return nombreValido;
    }
}

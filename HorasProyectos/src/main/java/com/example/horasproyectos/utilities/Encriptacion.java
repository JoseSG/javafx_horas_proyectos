package com.example.horasproyectos.utilities;

import javafx.scene.image.WritableImage;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;

import org.apache.commons.io.IOUtils;
import javafx.embed.swing.SwingFXUtils;

import javax.imageio.ImageIO;

public class Encriptacion {

    public static final String URL_IMAGENES_DECODIFICADAS = "./src/main/resources/com/example/horasproyectos/copias/";

    public static File imagenCodificada = null;
    public static String base64 = "";

    /**
     * Este método decodifica el archivo de una imagen que le llega (debe estar encriptado en Base64 con extensión .txt) y devuelve un
     * objeto WritableImage que podemos utilizar para pasárselo a un ImageView y mostrar la imagen que estaba codificada
     */
    public static WritableImage decodificar(File archivo) throws IOException {

        String fileAsString;

        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {

            String line = br.readLine();
            StringBuilder sb = new StringBuilder();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            fileAsString = sb.toString();
        }

        byte[] fileBytes = Base64.getMimeDecoder().decode(fileAsString.getBytes());

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(fileBytes);
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(byteArrayInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert bufferedImage != null;
        WritableImage convertedImage = SwingFXUtils.toFXImage(bufferedImage, null);
        return convertedImage;
    }


    /**
     * Este método codifica a Base 64 el archivo original que le pasemos como parámetro y devuelve otro archivo que se corresponde con
     * el original pero encriptado
     */
    public static File codificar(File archivoOriginal, String url, File fileCodificado) throws IOException {

        //Pasamos el archivo original a un array de bytes
        FileInputStream myStream = new FileInputStream(archivoOriginal);
        byte[] fileInBytes = IOUtils.toByteArray(myStream);
        //y lo codificamos a un String en Base 64
        base64 = Base64.getMimeEncoder().encodeToString(fileInBytes);

        //Inicializamos el archivo codificado en la url que nos llega como parámetro
        fileCodificado = new File(url);

        //Esta es la forma más eficiente de realizar el proceso de escritura del archivo codificado en base 64

        //Y escribimos en el archivo con la cadena codificada en Base64 para encriptarlo
        try (FileWriter fw = new FileWriter(fileCodificado);
             BufferedWriter bf = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bf)) {
            out.print(base64);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Decimos que sean iguales estos dos File y devolvemos el archivo ya codificado que nos pasaron como parámetro
        imagenCodificada = fileCodificado;
        return fileCodificado;
    }

    /**
     * Este método escribe el archivo que le pases como parámetro, que será una copia del array de bytes que le pases,
     * para que se pueda reproducirse, leerse, o guardarle datos dentro.
     */
    public static void generaCopiaEscrita(byte[] fileBytes, File file) {
        try {
            FileOutputStream fout = new FileOutputStream(file);
            fout.write(fileBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

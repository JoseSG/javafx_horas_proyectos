package com.example.horasproyectos.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class BridgeAPI {

    final static int CONECTION_TIMEOUT = 1000;
    final static int READ_TIMEOUT = 7000;

    /**Este método se encarga de abrir y cerrar la conexión al hacer una petición a la API*/
    public static String conection(String consulta) {

        String resultStream = "";

        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(consulta);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(CONECTION_TIMEOUT);
            httpURLConnection.setReadTimeout(READ_TIMEOUT);
            httpURLConnection.connect();
            resultStream = getData(httpURLConnection);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) httpURLConnection.disconnect();
        }
        return resultStream;
    }

    /**Este método se encarga de redirigir y recoger los datos de la consulta que se haya solicitado*/
    private static String getData(HttpURLConnection httpURLConnection) throws IOException {

        String resultStream = "";

        if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

            resultStream = readStream(httpURLConnection.getInputStream());
        }
        return resultStream;
    }

    /**Este método lee cada linea de la cadena que nos devuelve como resultado de la petición de datos*/
    private static String readStream(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        String nexLine = "";
        while ((nexLine = bufferedReader.readLine()) != null) {
            stringBuilder.append(nexLine);
        }
        return stringBuilder.toString();
    }
}

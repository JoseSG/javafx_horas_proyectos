package com.example.horasproyectos.utilities;

import com.example.horasproyectos.models.Empleado;
import com.example.horasproyectos.models.Participan;
import com.example.horasproyectos.models.Proyecto;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class Consultas {

    //TODO: Está claro que se podrían optimizar mucho las funciones de esta clase utilizando la herencia y el polimorfismo

    //Configuración API en host
    //public static String IP = "84.123.100.10";
    //public static String PUERTO = "9614";

    //Configuración API en cliente
    //public static String IP = "localhost";
    //public static String PUERTO = "8080";

    //TODO: Los valores de estas variables se obtienen en el constructor de esta clase, está aquí abajo
    public static String CONSULTA_LISTAR_PARTICIPAN = "";
    public static String CONSULTA_LISTAR_EMPLEADOS = "";
    public static String CONSULTA_LISTAR_PROYECTOS = "";

    public static String CONSULTA_COUNT_PARTICIPAN = "";

    public static String COMPLETA_CONSULTA_WITH_ALL = "";

    public static String CONSULTA_SUM_HORAS_EMPLEADO_BY_ID = "";
    public static String CONSULTA_SUM_HORAS_PROYECTO_BY_ID = "";
    public static String CONSULTA_SUM_HORAS_PARTICIPAN_BY_ID = "";


    //Constructor
    public Consultas(String IP, String PUERTO) {

        CONSULTA_LISTAR_PARTICIPAN = "http://" + IP +":" + PUERTO + "/participan/";
        CONSULTA_LISTAR_EMPLEADOS = "http://" + IP + ":" + PUERTO + "/empleados/";
        CONSULTA_LISTAR_PROYECTOS = "http://" + IP + ":" + PUERTO + "/proyectos/";

        CONSULTA_COUNT_PARTICIPAN = "http://" + IP + ":" + PUERTO + "/participan/count";

        COMPLETA_CONSULTA_WITH_ALL = "?withAll=true";

        CONSULTA_SUM_HORAS_EMPLEADO_BY_ID = CONSULTA_LISTAR_EMPLEADOS + "sum/";
        CONSULTA_SUM_HORAS_PROYECTO_BY_ID = CONSULTA_LISTAR_PROYECTOS + "sum/";
        CONSULTA_SUM_HORAS_PARTICIPAN_BY_ID = CONSULTA_LISTAR_PARTICIPAN + "sum/";
    }

    /**Este método elimina de la BD el empleado que le llegue como parámetro*/
    public static void deleteEmpleado(Empleado empleado){

        try {
            Unirest.delete(CONSULTA_LISTAR_EMPLEADOS + empleado.getEmpleadoid())
                    .header("Content-Type", "application/json")
                    .body("\t{\n\t\t\"empleadoid\": " + empleado.getEmpleadoid() +",\n\t\t\"nombreempleado\": \"" + empleado.getNombreempleado() +
                            "\",\n\t\t\"telefono\": \"" + empleado.getTelefono() + "\"\n\t}")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }


    /**Este método elimina de la BD el proyecto que le llegue como parámetro*/
    public static void deleteProyecto(Proyecto proyecto){

        try {
            Unirest.delete(CONSULTA_LISTAR_PROYECTOS + proyecto.getProyectoid())
                    .header("Content-Type", "application/json")
                    .body("\t{\n\t\t\"proyectoid\": " + proyecto.getProyectoid() +",\n\t\t\"nombre\": \"" + proyecto.getNombre() +
                            "\",\n\t\t\"descripcion\": \"" + proyecto.getDescripcion() + "\"\n\t}")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }


    /**Este método elimina de la BD el participan que le llegue como parámetro*/
    public static void deleteParticipan(Participan participan){

        try {
            Unirest.delete(CONSULTA_LISTAR_PARTICIPAN + participan.getRegistroid())
                    .header("Content-Type", "application/json")
                    .body("\t{\n\t\t\"registroid\": " + participan.getRegistroid() +",\n\t\t\"empleadoid\": \"" + participan.getEmpleadoid() +
                            "\",\n\t\t\"proyectoid\": \"" + participan.getProyectoid() + "\",\n\t\t\"horas\": \"" + participan.getHoras()
                            + "\"\n\t}")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }


    /**Este método modifica un registro de empleado*/
    public static void updateEmpleado(Empleado empleado, String nuevoNombre, String nuevoTelefono){

        try {
            Unirest.put(CONSULTA_LISTAR_EMPLEADOS + empleado.getEmpleadoid())
                    .header("Content-Type", "application/json")
                    .body("\t{\n\t\t\"empleadoid\": " + empleado.getEmpleadoid() +",\n\t\t\"nombreempleado\": \"" + nuevoNombre +
                            "\",\n\t\t\"telefono\": \"" + nuevoTelefono + "\"\n\t}")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }


    /**Este método modifica un registro participan*/
    public static void updateParticipan(Participan participan, String nuevoEmpleado, String nuevoProyecto, String nuevaHora){

        try {
            Unirest.put(CONSULTA_LISTAR_PARTICIPAN + participan.getRegistroid())
                    .header("Content-Type", "application/json")
                    .body("\t{\n\t\t\"registroid\": " + participan.getRegistroid() +",\n\t\t\"empleadoid\": \"" + nuevoEmpleado +
                            "\",\n\t\t\"proyectoid\": \"" + nuevoProyecto + "\",\n\t\t\"horas\": \"" + nuevaHora + "\"\n\t}")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
    /**Este método modifica un proyecto validado (que se muestra por pantalla)*/
    public static void updateProyecto(Proyecto proyecto, String nuevoNombre, String nuevaDescripcion) {

        try {
            Unirest.put(CONSULTA_LISTAR_PROYECTOS + proyecto.getProyectoid())
                    .header("Content-Type", "application/json")
                    .body("\t{\n\t\t\"proyectoid\": " + proyecto.getProyectoid() + ",\n\t\t\"nombre\": \"" + nuevoNombre +
                            "\",\n\t\t\"descripcion\": \"" + nuevaDescripcion + "\"\n\t}")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
    /**Este método crea un nuevo empleado (el que se muestra por pantalla)*/
    public static void createEmpleado (String nuevoNombre, String nuevoTelefono){

        try {
            Unirest.post(CONSULTA_LISTAR_EMPLEADOS)
                    .header("Content-Type", "application/json")
                    .body("\t{\n\t\t\"nombreempleado\": \"" + nuevoNombre +
                            "\",\n\t\t\"telefono\": \"" + nuevoTelefono + "\"\n\t}")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
        /**Este método crea un nuevo proyecto (que se muestra por pantalla)*/
        public static void createProyecto (String nuevoNombre, String nuevaDescripcion){

            try {
                Unirest.post(CONSULTA_LISTAR_PROYECTOS)
                        .header("Content-Type", "application/json")
                        .body("\t{\n\t\t\"nombre\": \"" + nuevoNombre +
                                "\",\n\t\t\"descripcion\": \"" + nuevaDescripcion + "\"\n\t}")
                        .asString();
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        }

        /**Este método crea un nuevo proyecto (que se muestra por pantalla)*/
        public static void createParticipan (Integer empleadoId, Integer proyectoId, String horas){

            try {
                Unirest.post(CONSULTA_LISTAR_PARTICIPAN)
                        .header("Content-Type", "application/json")
                        .body("\t{\n\t\t\"empleadoid\": \"" + empleadoId.toString() +
                                "\",\n\t\t\"proyectoid\": \"" + proyectoId.toString() +
                                "\",\n\t\t\"horas\": \"" + horas +
                                "\"\n\t}")
                        .asString();
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        }


    /**Devuelve una url usando un filtro dinámico por la cual obtendremos un listado de empleados filtrando por nombreempleado*/
    public static String findByNombreEmpleado(String nombre){

        return CONSULTA_LISTAR_EMPLEADOS + "?search=empleado.nombreempleado:" + nombre;
    }


    /**Devuelve una url de una query que filtra por id de empleado el total de horas que ha trabajado dicho empleado*/
    public static String findByIdHorasEmpleado(Integer idEmpleado){

        return CONSULTA_SUM_HORAS_EMPLEADO_BY_ID + idEmpleado;
    }


    /**Devuelve una url usando un filtro dinámico por la cual obtendremos un listado de proyectos filtrando por nombre*/
    public static String findByNombreProyecto(String nombre){

        return CONSULTA_LISTAR_PROYECTOS + "?search=proyecto.nombre:" + nombre;
    }


    /**Devuelve una url de una query que filtra por id de empleado el total de horas que ha trabajado dicho empleado*/
    public static String findByIdHorasProyecto(Integer idProyecto){

        return CONSULTA_SUM_HORAS_PROYECTO_BY_ID + idProyecto;
    }


    /**Devuelve una url de una query que filtra por id de participan y saca el total de horas que ha trabajado un empleado en un proyecto*/
    public static String findByIdHorasParticipan(Integer idEmpleado, Integer idProyecto){

        return CONSULTA_SUM_HORAS_PARTICIPAN_BY_ID + idEmpleado + "/" + idProyecto;
    }
}

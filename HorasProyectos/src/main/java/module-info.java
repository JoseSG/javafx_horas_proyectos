module com.example.horasproyectos {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.json;
    requires unirest.java;
    requires java.desktop;
    requires java.sql;
    requires mysql.connector.java;

    requires javafx.graphics;
    requires org.apache.commons.io;
    requires javafx.swing;


    opens com.example.horasproyectos to javafx.fxml;
    exports com.example.horasproyectos;
    exports com.example.horasproyectos.utilities;
    opens com.example.horasproyectos.utilities to javafx.fxml;
    exports com.example.horasproyectos.controllers;
    opens com.example.horasproyectos.controllers to javafx.fxml;
    exports com.example.horasproyectos.models;
    opens com.example.horasproyectos.models to javafx.fxml;
}